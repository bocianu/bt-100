program bt100test;

const 
    BMP_ADDRESS = $2500;
    PORTA = $d300;
    PORTB = $d301;
    
    IO_DEFAULT = %01110000;
    MOVE_LEFT =  %11101111;
    MOVE_RIGHT = %11011111;
    MOVE_PAPER = %10111111;

    PAPER_SENSOR    = %00001000;
    DOT_SENSOR      = %00000100;
    SYNC_SENSOR     = %00000010;
    HOME_SENSOR     = %00000001;
    

// joy 0 
// bit 0 - up - out 1 - czujnik wózka H
// bit 1 - down - out 6 - czujnik synchronizacji druku L
// bit 2 - left - out 7 - czujnik współrzednych punktu L
// bit 3 - right - out 3 - czujnik posuwu papieru L

// joy 1
// bit 4 - up - in 4 - wozek w lewo L
// bit 5 - down - in 1 - wozek w prawo L
// bit 6 - left - in 6 - posuw papieru L
// bit 7 - right - in 5 - głowica druku H


var PACTL:byte absolute $d302;
    x:byte;
    bmp: array [0..0] of byte absolute BMP_ADDRESS;
    i,ofs: word;
{$r resources.rc}

procedure RunMotor(mask:byte):assembler;
asm
    lda #IO_DEFAULT
    and mask
    sta PORTA
end;    

procedure StopMotors():assembler;
asm
    lda #IO_DEFAULT
    sta PORTA
end;    

procedure LineUp;
begin
    RunMotor(MOVE_PAPER);
    if (peek(PORTA) and PAPER_SENSOR) <> 0 then 
        repeat until (peek(PORTA) and PAPER_SENSOR) = 0;
    repeat until (peek(PORTA) and PAPER_SENSOR) <> 0;
    StopMotors;
end;

procedure LinesUp(lines:byte);
begin
    while lines > 0 do begin
        LineUp;
        Dec(lines);
    end;
end;

procedure MoveHeadLeft;
var add_dots:byte;
begin
    add_dots:=1;
    RunMotor(MOVE_LEFT);
    while (peek(PORTA) and HOME_SENSOR) = 0 do begin
        if ((peek(PORTA) and DOT_SENSOR) = 0) and (add_dots = 1) then add_dots:=0;
        if ((peek(PORTA) and DOT_SENSOR) <> 0) then add_dots:=1;
    end;
    StopMotors;
end;

procedure StartRightMove;
begin
    RunMotor(MOVE_RIGHT);
    repeat until (peek(PORTA) and HOME_SENSOR) = 0;
    repeat until (peek(PORTA) and SYNC_SENSOR) = 0;
    repeat until (peek(PORTA) and SYNC_SENSOR) <> 0;
end;

procedure Back2Left;
var dot2syn: integer;
    add_dots: byte;
    t:word;
begin
    t:=0;
    dot2syn := 0;
    add_dots := 1;
    
    while (peek(PORTA) and SYNC_SENSOR) <> 0 do begin
        if ((peek(PORTA) and DOT_SENSOR) = 0) and (add_dots = 1) then begin
            add_dots:=0;
            Inc(dot2syn);
        end;
        if ((peek(PORTA) and DOT_SENSOR) <> 0) then add_dots:=1;
        Inc(t);
        if t>64000 then break;
    end;
    
    StopMotors;
    LineUp;
    RunMotor(MOVE_LEFT);
    repeat until (peek(PORTA) and SYNC_SENSOR) = 0;
    repeat until (peek(PORTA) and SYNC_SENSOR) <> 0;
    add_dots := 0;
    
    while (dot2syn >= 0) do begin
        if ((peek(PORTA) and DOT_SENSOR) = 0) and (add_dots = 1) then begin
            add_dots:=0;
            Dec(dot2syn);
        end;
        if ((peek(PORTA) and DOT_SENSOR) <> 0) then add_dots:=1;
    end;
end;

procedure PrintByteRight(b:byte);
var i:byte;
    t:word;
    bit:byte;
    io:byte;
begin
    bit := %10000000;
    io := IO_DEFAULT and MOVE_RIGHT;
    for i:=0 to 7 do begin
        t:=0;
        while (peek(PORTA) and DOT_SENSOR) <> 0 do begin
            Inc(t);
            if t>64000 then break;
        end;
        if (b and bit) <> 0 then begin 
                Poke(PORTA, %10000000 or io);
                asm nop end;
                Poke(PORTA, io);
        end;
        bit := bit shr 1;
        while (peek(PORTA) and DOT_SENSOR) = 0 do begin
            Inc(t);
            if t>64000 then break;
        end;
    end;
end;

procedure PrintByteLeft(b:byte);
var i:byte;
    t:word;
    bit:byte;
    io:byte;
begin
    bit := %00000001;
    io := IO_DEFAULT and MOVE_LEFT;
    for i:=0 to 7 do begin
        t:=0;
        while (peek(PORTA) and DOT_SENSOR) = 0 do begin
            Inc(t);
            if t>32000 then break;
            if (peek(PORTA) and HOME_SENSOR) <> 0 then break;
        end;
        if (b and bit) <> 0 then begin
                Poke(PORTA, %10000000 or io);
                asm nop end;
                Poke(PORTA, io);
        end;
        bit := bit shl 1;
        while (peek(PORTA) and DOT_SENSOR) <> 0 do begin
            Inc(t);
            if t>32000 then break;
            if (peek(PORTA) and HOME_SENSOR) <> 0 then break
        end;
    end;
end;

procedure SetPIADir(dir:byte);
var val:byte;
begin
    val := PACTL and $FB;
    PACTL := val;
    Poke(PORTA,dir);
    PACTL := val or $04;
end;

//////////////////////////////////////////////////////////////////////////////////

begin
    SetPIADir(%11110000);

    Poke(PORTA, 0);
    Pause(10);
    Poke(PORTB,$fe);
    poke($d40e,0);
    asm sei end;

    LinesUp(20);

    Back2Left;

    ofs:=0;
    for i:=0 to 319 div 2 do begin
        MoveHeadLeft;
        StartRightMove;
        for x:=0 to 59 do PrintByteRight(bmp[ofs+x]);
        Back2Left;
        for x:=119 downto 60 do PrintByteLeft(bmp[ofs+x]);
        Inc(ofs,120);
        StopMotors;
        LineUp;
    end;
    LinesUp(10);
    StopMotors;
    asm cli end;
    poke($d40e,$40);
  
end.
